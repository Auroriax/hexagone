{
    "id": "bad4f37f-a854-4721-b03d-8446e0847db3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "SourceCodePro",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Source Code Pro",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b5bda6b9-48f1-4edf-afdb-2430f1530292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4fe5f029-9bf4-4b26-ab73-3724e6d8bbb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 169,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "33381fa6-8f15-4c93-ac42-5260505c2568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 160,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b6826cc5-04ae-4f2d-a41d-3db317bfa4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 150,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "138f4b51-3a68-4827-b4cc-6364e5037156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 140,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3290e9d6-c973-42be-9ddc-6b84363416df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1c1b8f76-fac1-4e7b-9691-b92d2c2a87ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 116,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "050f1834-1258-42d5-a389-a2be37c04e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 111,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "30ba7260-c63a-4f02-b0f5-cddf44d0f686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 104,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f6c6ad4e-1411-4177-a39e-d3588cc88802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 96,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "fa1cf60e-c88e-416d-b755-fdba39581794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 175,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b186601e-6e54-473b-99c2-13fc08a6a3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "79afb55f-b5a3-4f46-88bd-a95644f13727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 69,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f7dcd371-e56a-4fad-a7c2-ada0941c5ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 59,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ce8f7ba9-0bd8-479e-b05d-1eb902f6e8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 53,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0687f9c2-cd20-4562-80f7-900de0d19cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 43,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "89db0322-aa69-4272-baa0-714793058fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 33,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "442527f5-594f-4bef-98ca-d6f8399d892e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "eda2aa72-bee0-446c-bd68-b11f5bd9596c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8139a881-aba2-4d30-bc6f-a7a175b128f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "44a99b5b-d917-4ab2-9a9f-eccb73ca18b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 242,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e5c1a1e9-3f4c-48a5-9cf2-58326b180a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 75,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a65e3a73-395b-45ae-8dae-044b9566a2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 185,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "746b8dab-5a2a-44dc-a392-cc11613f54d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8405ea95-9bbf-46b7-891d-1bfc9ac9631d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 205,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bea5387f-36d1-4422-87f2-c77cfe97ae28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 155,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c4247fca-6fbb-44ef-ac7f-f8fc2aea6ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 149,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2fa4aafa-2bbf-4997-b650-53b774e53e0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "998fa7a3-aeb8-4c49-9f00-b9a412a867a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 134,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0a5254b0-f194-4a84-a0d6-b9086d0a4785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 124,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eb5f962a-037d-471e-8942-0e764918597a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 115,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6b316d65-8386-48ef-8965-37a5bfee2c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 106,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "91e4a11a-51b5-49d7-9c5e-929180052946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 95,
                "y": 68
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e3f05e94-9069-4c69-b76d-45df047e30ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 83,
                "y": 68
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "de2f3879-eb6c-4bc1-91a5-acfeca4ee99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 73,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7fe2700a-4a3e-46c4-8670-4d09b8c31d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cc642ab0-0873-4172-9458-570b80696275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0062d1a3-f685-471c-8c04-84e4e8ef3c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d1c27ef2-fcaa-44b4-94ae-1c1caaba1b0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 33,
                "y": 68
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "817208cf-f58c-44fa-96cf-3228fae9fbda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "845ac108-187f-4d2a-b93e-aea5dae55e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3f9ecdd-0242-41e3-92d4-dc719027daea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "52731ebe-9671-408e-a57b-7b099e6ef5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 245,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ec2a3ff6-ffee-47db-a7fd-00d2958e7b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 234,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d4427319-50c8-42dd-b875-1d90b0e6bf79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 225,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e81f074a-b6eb-40e3-a8d1-e2a2fd427781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 215,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "17bb33fb-af98-48c4-92ee-a54cb624f9c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8394da1e-36ed-458e-bc9f-d53373d42fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 221,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b17b1ba4-199a-4625-b5dc-4f2335f3fe11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 211,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4dc7d10d-4e88-4bea-b422-eb46613b7255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5fa377a9-763c-4a69-9aaf-9680c8d70aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9af84500-ae81-4cd7-8b5a-691d3d704bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "42566da5-79f7-4f39-b780-77b755192396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d8667fe1-a37c-418f-9d05-4fa89238cb37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cdabd37d-f81d-44f5-a9a2-1f10bcba6009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d0fae4e3-0c3d-4671-bfc7-17d8aa91fa0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f6ca03d6-582b-48cc-9d44-fd59f4c6f703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2c4c83f5-85f5-4d64-a446-0c3a7f4e4fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ac8f8c2d-70f0-4cb3-8d3c-aa135943c54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "489f8be8-eda2-47a7-936f-25afa48ba78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "460ea87c-0300-4e4d-89ac-96eecb5d1179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "52d9801e-bd8c-45fc-9436-b32edfe48c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fdc86f12-3398-4206-a3ec-0f0dfdb553b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4afdd902-715c-498c-8d02-107f1fe058fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b457516c-b73c-4667-bda6-bafe5301533b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "192edc64-f0ea-4173-9194-b823130083a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f0519ecd-60e0-410b-89ce-669f2022a675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2e72f50c-28bf-4468-b85a-857396ac75cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2ca16112-18f1-44ca-b280-23cd14522a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7059cd86-6b7b-49fe-a25b-d43d52fbefab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2928f4c7-90ab-4ee9-8e13-af10743f1347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4a376dff-0b59-4a80-9230-f2af174c16a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9e511f75-e102-479e-88b6-46bcfb0d0ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 89,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4ff7a977-9a88-4d69-972c-b980560af89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "61b5bb16-2a23-4379-bd91-1cec270ff3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 192,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "30ed58c7-6229-4599-8489-aeba1c8d53ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 181,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fc73b757-03b2-4f9a-a1ea-f27139c7e898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 171,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fdda5046-a559-4da4-b133-4149fbd89de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "56ca1c9c-c0e9-423b-817b-f62f4f5d3b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 150,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c574d985-bda5-43da-8fb7-a8dfd4f29247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b5e1a5da-fc73-4c35-96f8-856dfbbd1d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "62d7b85c-b7c2-4522-b25a-f3cc565ae2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 118,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9319198e-2a0e-45c3-8b56-18de49c917d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 109,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e372cdfd-ad4e-4706-aa08-7cccd129a8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "74c4fc8a-43b4-4307-9297-b139eea90a6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 99,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7db6f188-d9e2-4139-9c92-a4ae3e63d782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "aa77469c-f3b4-43e9-a920-240a07d3d234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "83e2e57e-1e82-43f2-81a5-463befa5cee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 56,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "761c3821-baf9-4899-ac87-4ab4a2b0985e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "58c15086-5db4-4d89-a495-92e0118e5e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 34,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1b830856-9d40-4fa1-9121-252cb2ef6e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9c9ea280-4750-4fe0-94d0-68b3d0054e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 15,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "60d2cdc1-053f-48c9-beaa-9f3ffb21e4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2f516d11-998b-4c02-88a4-e5d44363b30e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7af50b12-7508-40ef-b54b-525573869218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 165,
                "y": 68
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5754670c-e418-4a32-b5f8-36cd4057aa2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 175,
                "y": 68
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}