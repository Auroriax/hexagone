{
    "id": "2fc3543c-1a97-4acb-a02c-1985b20caec2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_copy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8b12878-ff01-426a-9a7c-faf8d0212532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc3543c-1a97-4acb-a02c-1985b20caec2",
            "compositeImage": {
                "id": "e1ce358c-6b8b-4d7c-85a2-5ce60802729e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b12878-ff01-426a-9a7c-faf8d0212532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "780f0900-9868-4b64-b7b5-6ae2137c6210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b12878-ff01-426a-9a7c-faf8d0212532",
                    "LayerId": "8bf70ab0-beb9-4948-9d97-c98966a28116"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8bf70ab0-beb9-4948-9d97-c98966a28116",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fc3543c-1a97-4acb-a02c-1985b20caec2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}