{
    "id": "da1fc981-69a1-4e7d-88f1-13156da25809",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_glyph_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe4da344-7de0-4096-ab19-0f5ec7938ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da1fc981-69a1-4e7d-88f1-13156da25809",
            "compositeImage": {
                "id": "004a7ad1-060f-468d-8e55-5becc2d8c803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4da344-7de0-4096-ab19-0f5ec7938ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97e7c4f-15b9-4442-a4dc-1e0001283728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4da344-7de0-4096-ab19-0f5ec7938ad7",
                    "LayerId": "d6a7576c-3885-4f8b-8c24-de7d8341806a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6a7576c-3885-4f8b-8c24-de7d8341806a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da1fc981-69a1-4e7d-88f1-13156da25809",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}