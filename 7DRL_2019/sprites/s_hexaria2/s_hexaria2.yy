{
    "id": "adf72e87-127e-4136-843d-37ef17ceb8ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hexaria2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36016c54-3e41-4c12-9552-8150c1fc9008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adf72e87-127e-4136-843d-37ef17ceb8ea",
            "compositeImage": {
                "id": "b3623143-a87c-46c7-a769-77348b5a7130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36016c54-3e41-4c12-9552-8150c1fc9008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81528bc3-85b4-40fa-9e35-48db12fc1488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36016c54-3e41-4c12-9552-8150c1fc9008",
                    "LayerId": "70a4b164-cfab-440d-ad10-b4b71c9dcab9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "70a4b164-cfab-440d-ad10-b4b71c9dcab9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adf72e87-127e-4136-843d-37ef17ceb8ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}