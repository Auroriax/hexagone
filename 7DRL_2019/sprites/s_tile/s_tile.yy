{
    "id": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4b64a20-576d-40e5-b79d-dd8e8a27a46a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
            "compositeImage": {
                "id": "bd6ba7ba-cdc7-4a30-afe2-209e32a9f94a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b64a20-576d-40e5-b79d-dd8e8a27a46a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d71a8e6-8346-4350-8e9b-ef1df5d566cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b64a20-576d-40e5-b79d-dd8e8a27a46a",
                    "LayerId": "ac637fad-9bdd-494e-b569-5906e46a3621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "ac637fad-9bdd-494e-b569-5906e46a3621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 18
}