{
    "id": "f09f1094-fe1d-4c08-a6d3-085abad074b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cross",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00755603-6ab9-4816-abb6-7663a486db0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f09f1094-fe1d-4c08-a6d3-085abad074b1",
            "compositeImage": {
                "id": "e0192aab-6a06-4bd4-b055-72b49d52d04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00755603-6ab9-4816-abb6-7663a486db0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19ada7be-1503-469a-8450-9a715cc694a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00755603-6ab9-4816-abb6-7663a486db0a",
                    "LayerId": "024e5df0-68c1-469a-8798-b8a5fb86154f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "024e5df0-68c1-469a-8798-b8a5fb86154f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f09f1094-fe1d-4c08-a6d3-085abad074b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}