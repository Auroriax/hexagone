{
    "id": "24530fea-d69e-4482-84c3-36d68d8a9878",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_trophy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9a61bca-8ea1-4477-92bb-3eac16c1869c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24530fea-d69e-4482-84c3-36d68d8a9878",
            "compositeImage": {
                "id": "3adcb629-4b79-4f46-b91b-0f6a5cfddd2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a61bca-8ea1-4477-92bb-3eac16c1869c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5eb3051-1fe3-4185-bcf8-2c9ca1902d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a61bca-8ea1-4477-92bb-3eac16c1869c",
                    "LayerId": "448fae81-b275-4766-9461-0f10cfcf78d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "448fae81-b275-4766-9461-0f10cfcf78d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24530fea-d69e-4482-84c3-36d68d8a9878",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}