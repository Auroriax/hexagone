{
    "id": "c55684d2-796c-4505-b1c3-495b4715264a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55551e4e-dd11-429d-b6e8-773c2eab9842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c55684d2-796c-4505-b1c3-495b4715264a",
            "compositeImage": {
                "id": "19a9f136-b3d0-4bff-916e-6d1f9c77d3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55551e4e-dd11-429d-b6e8-773c2eab9842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b82ee2-1181-487f-a08e-2c9788756a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55551e4e-dd11-429d-b6e8-773c2eab9842",
                    "LayerId": "26de3612-29c6-440b-8187-9fef105aed93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26de3612-29c6-440b-8187-9fef105aed93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c55684d2-796c-4505-b1c3-495b4715264a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}