{
    "id": "b2b03c4c-11a7-4a78-bf9d-dedc6cea1209",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_glyph_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3964f76-dad6-42b0-944f-8ac1b0887307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2b03c4c-11a7-4a78-bf9d-dedc6cea1209",
            "compositeImage": {
                "id": "45e02cb1-2b97-4f07-8d1e-2e095fecbae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3964f76-dad6-42b0-944f-8ac1b0887307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11155dd6-8448-45e3-9181-6d6f3971ab04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3964f76-dad6-42b0-944f-8ac1b0887307",
                    "LayerId": "a5897035-2ef3-45c5-bb44-0b96b8c832dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5897035-2ef3-45c5-bb44-0b96b8c832dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2b03c4c-11a7-4a78-bf9d-dedc6cea1209",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}