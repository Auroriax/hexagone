{
    "id": "8711a7fb-7a3a-457d-94b1-53f30bac7e91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hexaria3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b8e6a14-c797-4791-a294-616998654812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8711a7fb-7a3a-457d-94b1-53f30bac7e91",
            "compositeImage": {
                "id": "53fec19a-3533-4fe9-a93b-53e169b9113e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8e6a14-c797-4791-a294-616998654812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a48a32-f999-48a4-b8fe-feaa5252348d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8e6a14-c797-4791-a294-616998654812",
                    "LayerId": "be0b866a-12fe-4e6a-b69f-c15cfc79ede0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "be0b866a-12fe-4e6a-b69f-c15cfc79ede0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8711a7fb-7a3a-457d-94b1-53f30bac7e91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}