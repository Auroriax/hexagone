{
    "id": "5fee020a-afaf-459a-b554-fe8c3000d2b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_glyph_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d372b961-0f00-4e48-aad4-63c178fc14cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fee020a-afaf-459a-b554-fe8c3000d2b3",
            "compositeImage": {
                "id": "22a9b019-e9a8-415d-b71d-5141d3217932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d372b961-0f00-4e48-aad4-63c178fc14cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04e0417-2f80-4510-8e3f-6e235f730ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d372b961-0f00-4e48-aad4-63c178fc14cb",
                    "LayerId": "2f4d5f05-1ea6-458d-810f-97c846e73623"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f4d5f05-1ea6-458d-810f-97c846e73623",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fee020a-afaf-459a-b554-fe8c3000d2b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}