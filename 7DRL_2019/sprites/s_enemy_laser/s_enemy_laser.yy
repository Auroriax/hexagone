{
    "id": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 635,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 620,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1668c38a-d0b2-494a-88ca-e2bc331069fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
            "compositeImage": {
                "id": "11e93e3c-c997-4707-97c0-5869f79a2010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1668c38a-d0b2-494a-88ca-e2bc331069fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268ce943-e118-49fb-b9e5-2c39ef0f3e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1668c38a-d0b2-494a-88ca-e2bc331069fe",
                    "LayerId": "1e38429c-b89b-436b-8681-1f5b4245ee53"
                }
            ]
        },
        {
            "id": "69790f05-bfad-4a5a-be1c-eb14c82e0401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
            "compositeImage": {
                "id": "10bb9d1d-6c81-4762-ac96-9bd7e6e532a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69790f05-bfad-4a5a-be1c-eb14c82e0401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "883048e7-1a4c-445d-87cb-8e7be2851fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69790f05-bfad-4a5a-be1c-eb14c82e0401",
                    "LayerId": "1e38429c-b89b-436b-8681-1f5b4245ee53"
                }
            ]
        },
        {
            "id": "afa87c47-1fef-4376-b081-b565b47d4965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
            "compositeImage": {
                "id": "6035aebb-d68a-4882-ace9-4bc8f417a30f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa87c47-1fef-4376-b081-b565b47d4965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46ce09ef-67d2-463d-b683-23d5aaa7beac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa87c47-1fef-4376-b081-b565b47d4965",
                    "LayerId": "1e38429c-b89b-436b-8681-1f5b4245ee53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 642,
    "layers": [
        {
            "id": "1e38429c-b89b-436b-8681-1f5b4245ee53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 623
}