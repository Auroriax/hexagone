{
    "id": "6aa846b1-e70d-48dd-ab3e-538a577d0a82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b99ca5f-651f-43fc-9424-0106f3202b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aa846b1-e70d-48dd-ab3e-538a577d0a82",
            "compositeImage": {
                "id": "aa676ef9-f9be-4582-9086-e5c8b48ea972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b99ca5f-651f-43fc-9424-0106f3202b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659cbfb5-38df-4a84-81e5-b12bf01cfaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b99ca5f-651f-43fc-9424-0106f3202b2a",
                    "LayerId": "f752be2a-9a29-48a1-bb58-c00b1363726d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f752be2a-9a29-48a1-bb58-c00b1363726d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aa846b1-e70d-48dd-ab3e-538a577d0a82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}