{
    "id": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64a8b55d-cfff-42b7-a75a-abedacdcc539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
            "compositeImage": {
                "id": "5b5d59f1-e6e6-4916-a8de-8df0d98fc2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a8b55d-cfff-42b7-a75a-abedacdcc539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9977c9-dd0b-4114-bfaa-03e7bc31e985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a8b55d-cfff-42b7-a75a-abedacdcc539",
                    "LayerId": "43196a7f-5318-4d06-b819-4dc6f4cf8a48"
                }
            ]
        },
        {
            "id": "42f8aa6b-676e-4e0c-9399-66031651e840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
            "compositeImage": {
                "id": "8404ee02-086a-4f3d-b0a4-812d51183beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f8aa6b-676e-4e0c-9399-66031651e840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b45b387-a416-47cf-b136-2c12fd15166e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f8aa6b-676e-4e0c-9399-66031651e840",
                    "LayerId": "43196a7f-5318-4d06-b819-4dc6f4cf8a48"
                }
            ]
        },
        {
            "id": "fe63aa60-b8cb-4914-b3f5-bdfbf0c785a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
            "compositeImage": {
                "id": "955bbf80-380c-4107-af9a-6157a1f02b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe63aa60-b8cb-4914-b3f5-bdfbf0c785a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b52e83-18fe-42af-9b0d-f54ad9db9ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe63aa60-b8cb-4914-b3f5-bdfbf0c785a6",
                    "LayerId": "43196a7f-5318-4d06-b819-4dc6f4cf8a48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "43196a7f-5318-4d06-b819-4dc6f4cf8a48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}