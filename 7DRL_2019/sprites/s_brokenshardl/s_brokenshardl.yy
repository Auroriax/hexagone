{
    "id": "f3ca5b73-9818-4915-b3fd-f51094a0851f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_brokenshardl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f3bc245-e3dc-4ecd-b6c8-72c05ab02bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3ca5b73-9818-4915-b3fd-f51094a0851f",
            "compositeImage": {
                "id": "38a00566-aa25-4528-bcca-3cfd42951141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3bc245-e3dc-4ecd-b6c8-72c05ab02bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7deee684-2665-47c6-9537-af2fc9a60f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3bc245-e3dc-4ecd-b6c8-72c05ab02bd6",
                    "LayerId": "cac5c74a-d214-4ecb-bac0-e44996f44ad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "cac5c74a-d214-4ecb-bac0-e44996f44ad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3ca5b73-9818-4915-b3fd-f51094a0851f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 7
}