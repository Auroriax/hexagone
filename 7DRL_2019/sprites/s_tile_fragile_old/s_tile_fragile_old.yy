{
    "id": "d421789f-807c-4e55-9836-9fefab3b3868",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tile_fragile_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4673dfc-82e0-4766-b62e-ce645bba21d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d421789f-807c-4e55-9836-9fefab3b3868",
            "compositeImage": {
                "id": "e0c1f5da-d57e-46f6-9870-e1948709d4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4673dfc-82e0-4766-b62e-ce645bba21d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671b748f-5f1f-4ced-a9a4-35e462e8be34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4673dfc-82e0-4766-b62e-ce645bba21d5",
                    "LayerId": "411ac28d-e368-497c-9924-6824b618a9da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "411ac28d-e368-497c-9924-6824b618a9da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d421789f-807c-4e55-9836-9fefab3b3868",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 18
}