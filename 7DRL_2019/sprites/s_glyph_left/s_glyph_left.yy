{
    "id": "7035d346-214b-46b2-8411-55cbb603894e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_glyph_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8cdd9df-14f0-4edf-a120-a384050f87b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7035d346-214b-46b2-8411-55cbb603894e",
            "compositeImage": {
                "id": "3a21c069-f4e2-4807-aacd-642c5626d078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cdd9df-14f0-4edf-a120-a384050f87b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed79fac0-b253-4902-88ca-f2144ddf7bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cdd9df-14f0-4edf-a120-a384050f87b5",
                    "LayerId": "fc447790-236f-4018-a1bc-092787c524b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc447790-236f-4018-a1bc-092787c524b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7035d346-214b-46b2-8411-55cbb603894e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}