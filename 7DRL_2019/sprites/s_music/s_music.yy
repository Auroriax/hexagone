{
    "id": "af64833f-02b3-4af6-9bec-2ee528a1dd6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_music",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35926a9b-03d5-4e11-9b6e-02e4dae70a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af64833f-02b3-4af6-9bec-2ee528a1dd6d",
            "compositeImage": {
                "id": "68b6e0bc-0196-4aec-8bd8-63a76bc4cdc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35926a9b-03d5-4e11-9b6e-02e4dae70a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0fa59d-7574-4cb6-8b4a-8e4ac16ba2c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35926a9b-03d5-4e11-9b6e-02e4dae70a41",
                    "LayerId": "9f09202d-15d3-4788-9da3-419544585ae4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f09202d-15d3-4788-9da3-419544585ae4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af64833f-02b3-4af6-9bec-2ee528a1dd6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}