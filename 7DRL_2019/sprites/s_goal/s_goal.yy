{
    "id": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_goal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a56cd259-5c05-4bab-a82f-ecc2866d90be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
            "compositeImage": {
                "id": "da0c3ca9-a2c9-4b4d-af88-fcbbb9b0fd20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56cd259-5c05-4bab-a82f-ecc2866d90be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670171f0-ffc5-4938-bee1-5bb27d298be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56cd259-5c05-4bab-a82f-ecc2866d90be",
                    "LayerId": "ad7fe1bb-13cf-42ab-aa7b-a3560f661fb7"
                }
            ]
        },
        {
            "id": "a6e0d97d-0809-4dda-a93d-4a34f37cd7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
            "compositeImage": {
                "id": "4469ab2c-f896-48b5-a8b3-335cb983b840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e0d97d-0809-4dda-a93d-4a34f37cd7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e81793-04ac-41e2-9cf9-64688a877129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e0d97d-0809-4dda-a93d-4a34f37cd7ea",
                    "LayerId": "ad7fe1bb-13cf-42ab-aa7b-a3560f661fb7"
                }
            ]
        },
        {
            "id": "f13176d8-f01c-413f-b2f3-b02bcf7971ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
            "compositeImage": {
                "id": "10ddf004-e226-4532-8982-649f3b321df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13176d8-f01c-413f-b2f3-b02bcf7971ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1950250a-c397-400f-a059-3482d1e7f7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13176d8-f01c-413f-b2f3-b02bcf7971ec",
                    "LayerId": "ad7fe1bb-13cf-42ab-aa7b-a3560f661fb7"
                }
            ]
        },
        {
            "id": "19fd796b-7b84-4b6c-8de8-a2bb1a8511a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
            "compositeImage": {
                "id": "687b015a-35e9-421c-a7ca-7d296c4d2478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fd796b-7b84-4b6c-8de8-a2bb1a8511a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39eddd9-d5a5-46f7-bb02-564956ec35d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fd796b-7b84-4b6c-8de8-a2bb1a8511a6",
                    "LayerId": "ad7fe1bb-13cf-42ab-aa7b-a3560f661fb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad7fe1bb-13cf-42ab-aa7b-a3560f661fb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 45
}