{
    "id": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad828d99-3bd1-46f6-8d99-19988e40e61e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "36a156c4-25a8-4e44-a8ce-619b6456adb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad828d99-3bd1-46f6-8d99-19988e40e61e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d98f8b5a-4532-4790-a575-986854fd9cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad828d99-3bd1-46f6-8d99-19988e40e61e",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "e8b5a550-ec74-4fbf-8ad2-873ce5b97fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "3773de70-64ec-46d0-8341-7a5ad05629f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b5a550-ec74-4fbf-8ad2-873ce5b97fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1719bd20-c0e4-4cd1-ad15-d79f29115718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b5a550-ec74-4fbf-8ad2-873ce5b97fd8",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "9b0bc7aa-8b1b-465a-8af7-2ac6a7196d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "8f1c1003-efd5-43f7-85a2-3fdc7421a141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0bc7aa-8b1b-465a-8af7-2ac6a7196d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b0e2f0-7608-4332-bb81-c8998ee33aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0bc7aa-8b1b-465a-8af7-2ac6a7196d4f",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "5804c7d8-7b3a-49d8-8b55-ca8c8f369fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "87f467fd-afef-4ba5-a0b9-8736f9058612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5804c7d8-7b3a-49d8-8b55-ca8c8f369fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d7b25bc-9177-4ec6-a682-e3a9499f32b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5804c7d8-7b3a-49d8-8b55-ca8c8f369fbe",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "0ab78479-28c9-4956-a3a8-9616f6c0feaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "fb76e30c-5326-4dba-8fc0-bbd80bb0a853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab78479-28c9-4956-a3a8-9616f6c0feaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4105b45a-0ef1-46fd-ab07-53401db0b717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab78479-28c9-4956-a3a8-9616f6c0feaa",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "e0b1a3d0-1439-43c2-927a-95cf03452b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "c0d0339d-2868-40c9-a4b5-42337ca7ed32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b1a3d0-1439-43c2-927a-95cf03452b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218dbeac-a390-4e74-ab73-f8d6e215beab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b1a3d0-1439-43c2-927a-95cf03452b6c",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        },
        {
            "id": "bb1b6bfa-378c-40b9-a8f5-4da0e1a54bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "compositeImage": {
                "id": "484ec639-e567-4b1b-8453-89f78f966f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1b6bfa-378c-40b9-a8f5-4da0e1a54bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e312bd8-34d2-4b0f-8469-326cc2738a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1b6bfa-378c-40b9-a8f5-4da0e1a54bfd",
                    "LayerId": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b0e319e0-08f9-4bfd-b3a6-df78f91e0ce8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}