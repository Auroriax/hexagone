{
    "id": "491c2b2f-f6a2-4dec-8ebf-7e778909934e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_nomask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": -9000,
    "bbox_left": -9000,
    "bbox_right": -9000,
    "bbox_top": -9000,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2dd8a8b-e4f0-49ef-96cd-e055393ba809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "491c2b2f-f6a2-4dec-8ebf-7e778909934e",
            "compositeImage": {
                "id": "872a5855-1c1b-4b5c-9520-855f386f08bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2dd8a8b-e4f0-49ef-96cd-e055393ba809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b4e72c-b534-4152-97c7-736820172bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2dd8a8b-e4f0-49ef-96cd-e055393ba809",
                    "LayerId": "a1c76d79-7c5e-46d2-9510-b70dd53bc03f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a1c76d79-7c5e-46d2-9510-b70dd53bc03f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "491c2b2f-f6a2-4dec-8ebf-7e778909934e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}