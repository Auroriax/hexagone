{
    "id": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tilegrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c387870-4ade-4173-9e09-8532f06b26c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
            "compositeImage": {
                "id": "27039ae4-7c19-473e-a184-931cb1d79584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c387870-4ade-4173-9e09-8532f06b26c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e00faaa6-f8b1-441e-9274-10dcaab72f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c387870-4ade-4173-9e09-8532f06b26c9",
                    "LayerId": "845b9571-d351-4c62-831a-fdd5fda23283"
                }
            ]
        },
        {
            "id": "72ee6a8a-8292-46ee-bb86-3e6ade646bb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
            "compositeImage": {
                "id": "bc0b7ad0-3f4a-4dfc-9312-8477799b36a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ee6a8a-8292-46ee-bb86-3e6ade646bb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd388534-2c83-4c38-83cb-c44219f84d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ee6a8a-8292-46ee-bb86-3e6ade646bb0",
                    "LayerId": "845b9571-d351-4c62-831a-fdd5fda23283"
                }
            ]
        },
        {
            "id": "6535401e-4358-451d-98de-53b7d7b720e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
            "compositeImage": {
                "id": "08c7f244-1f46-4a9e-a776-0b0e029df155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6535401e-4358-451d-98de-53b7d7b720e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb2f1bf-1e0f-41fc-843f-0183de955519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6535401e-4358-451d-98de-53b7d7b720e1",
                    "LayerId": "845b9571-d351-4c62-831a-fdd5fda23283"
                }
            ]
        },
        {
            "id": "51959aca-4143-484e-83f9-ec4f883e817d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
            "compositeImage": {
                "id": "7125b0ac-a38c-437f-ba79-794b937d5dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51959aca-4143-484e-83f9-ec4f883e817d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b564de-296d-4d0d-832a-01775f25aaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51959aca-4143-484e-83f9-ec4f883e817d",
                    "LayerId": "845b9571-d351-4c62-831a-fdd5fda23283"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "845b9571-d351-4c62-831a-fdd5fda23283",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 18
}