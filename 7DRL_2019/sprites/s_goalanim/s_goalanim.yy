{
    "id": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_goalanim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3c63a70-18ca-45df-af22-3b3c9db8618c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "6ea52e38-726d-4b1b-af84-efeaaa712443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3c63a70-18ca-45df-af22-3b3c9db8618c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85cd2fd-544f-4033-93ca-4cb9d481f530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3c63a70-18ca-45df-af22-3b3c9db8618c",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "d74d32de-45b5-48f7-8518-73e0298d6aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "371f6c2b-eb47-4472-a44c-9f8d43f10d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74d32de-45b5-48f7-8518-73e0298d6aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f0ee99-f220-4aa1-9b36-052fcc92b798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74d32de-45b5-48f7-8518-73e0298d6aa2",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "97bc5a75-422f-4802-96a8-4a1adfb088c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "64be1b99-4994-42f2-bdd9-9268f5b9a707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97bc5a75-422f-4802-96a8-4a1adfb088c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b41b57a-b910-42be-81e5-6a2a5a99c3f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97bc5a75-422f-4802-96a8-4a1adfb088c1",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "d97020f6-ae33-4dbd-8101-db76422946ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "2df4a4f7-6974-46cf-b60c-8eed795c2c62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97020f6-ae33-4dbd-8101-db76422946ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d212e53-f486-41be-bff6-faf1478d1d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97020f6-ae33-4dbd-8101-db76422946ef",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "7c60c5ce-7982-4694-a5dc-0cade6d1eca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "a8feea32-0d3b-4a85-acf6-0238f7d14e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c60c5ce-7982-4694-a5dc-0cade6d1eca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec698fba-c277-42bd-a03f-afec52c035e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c60c5ce-7982-4694-a5dc-0cade6d1eca7",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "7b640a20-c7dc-4831-b856-36f353b184b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "0b6c3653-ca03-4a27-b40e-e7e97b2e0403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b640a20-c7dc-4831-b856-36f353b184b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8051ef8f-b991-4010-8392-904deff02a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b640a20-c7dc-4831-b856-36f353b184b5",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "c8b313c6-c234-4dd5-8a24-134d993f54a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "211f6793-90df-455c-a420-71610b41dff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b313c6-c234-4dd5-8a24-134d993f54a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b22428c-c72f-4e2c-8c35-a214b97ed234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b313c6-c234-4dd5-8a24-134d993f54a4",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "ab0e5abd-aa74-4a43-aed8-b16adf2c77bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "607d3be5-7b06-452d-b42f-6f0027ea7d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab0e5abd-aa74-4a43-aed8-b16adf2c77bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eae7c38-438b-425c-a452-8aadb66ddb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab0e5abd-aa74-4a43-aed8-b16adf2c77bc",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "638a9179-afc3-48cd-bcc0-4aa7602bf1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "ab801348-3693-4ec0-a5ce-4e075e482c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638a9179-afc3-48cd-bcc0-4aa7602bf1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae737b70-e316-45ed-88ba-69cd77e4847e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638a9179-afc3-48cd-bcc0-4aa7602bf1c0",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "92841008-1cc5-4014-9324-081e021c2805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "8df36211-0b8a-48b7-aa18-54c50a880be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92841008-1cc5-4014-9324-081e021c2805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30214d8f-c56a-45d1-a513-8700949cd38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92841008-1cc5-4014-9324-081e021c2805",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "51bb6541-bded-4c8d-bd11-475aedb51f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "685475dd-a704-4c48-9833-c75fbdd6001e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51bb6541-bded-4c8d-bd11-475aedb51f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d065f7-5834-4083-ba54-236b78232b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51bb6541-bded-4c8d-bd11-475aedb51f02",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "e380bee5-64ea-4249-a9bc-4abdf9f9e702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "c39caaa8-55c0-4c54-81bd-393d683e864f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e380bee5-64ea-4249-a9bc-4abdf9f9e702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b43abbd-7447-4bd6-ab8e-73b36c37efe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e380bee5-64ea-4249-a9bc-4abdf9f9e702",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "28b25bf2-05a9-44a4-b2f1-8a115f124c55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "f8b2db47-806c-4184-97f7-17a3ad83a9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b25bf2-05a9-44a4-b2f1-8a115f124c55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8cf320-b28a-4f4a-add4-275229a3828c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b25bf2-05a9-44a4-b2f1-8a115f124c55",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "421b4135-9cf6-47b4-801d-c36036251412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "b07e5d5d-d9a1-4ac9-aa7f-62530427cd0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421b4135-9cf6-47b4-801d-c36036251412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d753f3-207f-4fd9-a1d1-f42d194a7486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421b4135-9cf6-47b4-801d-c36036251412",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "e10e7f62-6f49-4145-a527-75d14c3ebf1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "8063a43c-b569-469c-b9ba-8813ab5d0445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e10e7f62-6f49-4145-a527-75d14c3ebf1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae708335-63e2-48e4-bcf7-5e3e3e3054f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e10e7f62-6f49-4145-a527-75d14c3ebf1f",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "cc16088c-73fb-48fd-a42e-22756a6f183f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "7f6ce886-e719-4af1-818b-9f37817fedb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc16088c-73fb-48fd-a42e-22756a6f183f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a86fb4d-b2dc-49ce-881e-ddc51224b4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc16088c-73fb-48fd-a42e-22756a6f183f",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "25d4fe13-a7e6-48d1-9d7e-b9a22df52d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "bab63028-00b3-4e61-98fd-887a460d46e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d4fe13-a7e6-48d1-9d7e-b9a22df52d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d080399-6fe2-42dc-b52c-82b6654b03bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d4fe13-a7e6-48d1-9d7e-b9a22df52d23",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        },
        {
            "id": "6d93dc93-6dc0-4028-8eb9-51c0d3d334b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "compositeImage": {
                "id": "71422be3-a9e8-4764-a719-0502eb3beb71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d93dc93-6dc0-4028-8eb9-51c0d3d334b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff3dcdc-bd59-4edd-9f4b-5d562839721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d93dc93-6dc0-4028-8eb9-51c0d3d334b8",
                    "LayerId": "ce85e787-fb13-4a93-8056-650573ea87da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "ce85e787-fb13-4a93-8056-650573ea87da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3000f20e-c2a9-4818-ba76-32bcf6f538ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 15
}