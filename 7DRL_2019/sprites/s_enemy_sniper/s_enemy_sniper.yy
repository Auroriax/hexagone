{
    "id": "90b32b23-0985-409d-a4f1-9a6e90c8cc71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_sniper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d997071-de04-4e10-bc47-19dfe6a03a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b32b23-0985-409d-a4f1-9a6e90c8cc71",
            "compositeImage": {
                "id": "e4545308-0864-47fe-ad94-f6c78e88cc5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d997071-de04-4e10-bc47-19dfe6a03a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fae18b4-cba7-4f72-bf74-9540455c6750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d997071-de04-4e10-bc47-19dfe6a03a2e",
                    "LayerId": "3b3472eb-65ea-43ab-92d0-05e8f1d91280"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b3472eb-65ea-43ab-92d0-05e8f1d91280",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90b32b23-0985-409d-a4f1-9a6e90c8cc71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}