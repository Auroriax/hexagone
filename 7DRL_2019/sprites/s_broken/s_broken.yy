{
    "id": "b8d2df2a-eef1-448d-988f-d1038f5b98bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_broken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 10,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1efc44cd-7f1d-4b17-b7ee-bd5dec42d866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8d2df2a-eef1-448d-988f-d1038f5b98bf",
            "compositeImage": {
                "id": "4ad91ff2-3013-4043-860d-6972a406a8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1efc44cd-7f1d-4b17-b7ee-bd5dec42d866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2338b9-edb1-4fbc-aaaa-1180aa489db0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1efc44cd-7f1d-4b17-b7ee-bd5dec42d866",
                    "LayerId": "f5e30ef4-5c9a-4115-8c9a-f16d1f887718"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "f5e30ef4-5c9a-4115-8c9a-f16d1f887718",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8d2df2a-eef1-448d-988f-d1038f5b98bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 19
}