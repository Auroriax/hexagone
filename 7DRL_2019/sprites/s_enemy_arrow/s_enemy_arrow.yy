{
    "id": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16a55b2a-71f3-4f36-abd8-0e37ac1e433d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
            "compositeImage": {
                "id": "8a728936-d9e6-45c4-b1d8-16d4cc40e533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a55b2a-71f3-4f36-abd8-0e37ac1e433d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d02065-94e9-4f5c-af3d-771831945097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a55b2a-71f3-4f36-abd8-0e37ac1e433d",
                    "LayerId": "f1230eaf-cbb0-4961-bc70-ec97236fab85"
                }
            ]
        },
        {
            "id": "24a469f8-6e72-4d8e-82da-1bfe58f57677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
            "compositeImage": {
                "id": "67224226-8e67-48e5-9cb4-afc89acbdc61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a469f8-6e72-4d8e-82da-1bfe58f57677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c57ed104-1d1c-4c66-b296-1a714c44d8f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a469f8-6e72-4d8e-82da-1bfe58f57677",
                    "LayerId": "f1230eaf-cbb0-4961-bc70-ec97236fab85"
                }
            ]
        },
        {
            "id": "5fc15e20-44e4-4b8b-80d4-ffc882419834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
            "compositeImage": {
                "id": "551acde9-dcf4-4312-ba73-6a21f775562d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fc15e20-44e4-4b8b-80d4-ffc882419834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49886681-3872-409c-bf32-93a54deba15c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fc15e20-44e4-4b8b-80d4-ffc882419834",
                    "LayerId": "f1230eaf-cbb0-4961-bc70-ec97236fab85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f1230eaf-cbb0-4961-bc70-ec97236fab85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}