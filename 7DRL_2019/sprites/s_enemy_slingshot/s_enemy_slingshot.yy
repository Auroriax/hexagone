{
    "id": "efbf0dd1-5896-4bbd-8ea5-d71432a06ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_slingshot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc49c373-d34a-4ecf-b296-f1784643855c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efbf0dd1-5896-4bbd-8ea5-d71432a06ca1",
            "compositeImage": {
                "id": "a9438a30-4b5c-4712-ab6d-1d84d775fbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc49c373-d34a-4ecf-b296-f1784643855c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8dff046-fc96-45ec-bb13-bcf3f7d82e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc49c373-d34a-4ecf-b296-f1784643855c",
                    "LayerId": "4fec18eb-02b8-4344-930f-b80f6cc1a078"
                }
            ]
        },
        {
            "id": "916594f5-f5b4-4cc4-a8bf-04a1c35c4dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efbf0dd1-5896-4bbd-8ea5-d71432a06ca1",
            "compositeImage": {
                "id": "237f71ab-85ed-480b-84af-3b917fff1c86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916594f5-f5b4-4cc4-a8bf-04a1c35c4dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c019ecb-69f1-4426-a237-b4a89f59b2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916594f5-f5b4-4cc4-a8bf-04a1c35c4dde",
                    "LayerId": "4fec18eb-02b8-4344-930f-b80f6cc1a078"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4fec18eb-02b8-4344-930f-b80f6cc1a078",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efbf0dd1-5896-4bbd-8ea5-d71432a06ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}