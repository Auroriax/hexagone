{
    "id": "5821dc6b-bc21-4636-9897-2062903784a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5efbc0e1-255e-429d-8dc3-0fe3496000bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5821dc6b-bc21-4636-9897-2062903784a1",
            "compositeImage": {
                "id": "b47c7554-d3dc-42b6-994a-36d4d519e9a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5efbc0e1-255e-429d-8dc3-0fe3496000bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cfa43b7-99f0-49a9-a8e1-686123f78ecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5efbc0e1-255e-429d-8dc3-0fe3496000bd",
                    "LayerId": "a1435815-d800-45ef-9c21-1a4ddf6928cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "a1435815-d800-45ef-9c21-1a4ddf6928cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5821dc6b-bc21-4636-9897-2062903784a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 18
}