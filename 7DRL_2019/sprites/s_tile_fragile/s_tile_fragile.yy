{
    "id": "de4594f0-1bfa-4298-95e9-abeed16dec97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tile_fragile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c697fec-762e-4dd6-a659-7718f682afee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4594f0-1bfa-4298-95e9-abeed16dec97",
            "compositeImage": {
                "id": "6034deca-d28f-4341-9d76-f8ae3ea17352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c697fec-762e-4dd6-a659-7718f682afee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "974f4010-cb23-4a31-a2e3-461dcdbab016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c697fec-762e-4dd6-a659-7718f682afee",
                    "LayerId": "9f3fd79d-61f9-4d2f-8e02-b3e10692667a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "9f3fd79d-61f9-4d2f-8e02-b3e10692667a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de4594f0-1bfa-4298-95e9-abeed16dec97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 18
}