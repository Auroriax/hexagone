{
    "id": "64decc1a-8b87-4725-bc33-d120da253904",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04c943bb-51bc-4d03-a61d-3c32faa75727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64decc1a-8b87-4725-bc33-d120da253904",
            "compositeImage": {
                "id": "96a0d90c-6d9b-4bd5-b9db-ec4e8fdf2b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c943bb-51bc-4d03-a61d-3c32faa75727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05d7e1a-3767-437d-8893-788989aa4108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c943bb-51bc-4d03-a61d-3c32faa75727",
                    "LayerId": "578fcd4b-c128-4eb9-a3f1-30ef0ae11ac2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "578fcd4b-c128-4eb9-a3f1-30ef0ae11ac2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64decc1a-8b87-4725-bc33-d120da253904",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}