{
    "id": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_defeattwinkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a536d57-bd19-4c8d-8d88-6aa8be14b49a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "compositeImage": {
                "id": "5c841d8d-cc15-4681-885f-68cdc24eaabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a536d57-bd19-4c8d-8d88-6aa8be14b49a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48252426-1555-41d5-82e8-c0de362b8769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a536d57-bd19-4c8d-8d88-6aa8be14b49a",
                    "LayerId": "3d336d3f-0b16-42b6-963a-0ba51fdf798f"
                }
            ]
        },
        {
            "id": "5e22faca-bcba-407a-afd8-11e7bb68b363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "compositeImage": {
                "id": "ae8d5a93-a033-4010-ab8d-662d1b401eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e22faca-bcba-407a-afd8-11e7bb68b363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95def82f-efb5-4849-9e29-73c062344ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e22faca-bcba-407a-afd8-11e7bb68b363",
                    "LayerId": "3d336d3f-0b16-42b6-963a-0ba51fdf798f"
                }
            ]
        },
        {
            "id": "799361c5-a807-4a28-8bd1-d0bea9f6c184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "compositeImage": {
                "id": "afa2be19-3481-4a68-8005-b028d03a91a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799361c5-a807-4a28-8bd1-d0bea9f6c184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b0f0f9-0ee1-4ed0-896b-eb139136b1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799361c5-a807-4a28-8bd1-d0bea9f6c184",
                    "LayerId": "3d336d3f-0b16-42b6-963a-0ba51fdf798f"
                }
            ]
        },
        {
            "id": "e5d78d65-0dd4-4551-8433-42488c41757d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "compositeImage": {
                "id": "59067343-c7c8-4b83-a54c-9e9315bfbd93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d78d65-0dd4-4551-8433-42488c41757d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17533ec2-ca5e-4523-8404-c584fbc4e7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d78d65-0dd4-4551-8433-42488c41757d",
                    "LayerId": "3d336d3f-0b16-42b6-963a-0ba51fdf798f"
                }
            ]
        },
        {
            "id": "97f3fed3-cb12-4544-88e0-4ba7cd68b88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "compositeImage": {
                "id": "c56f9284-74b9-4830-8339-ce9cb7850578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f3fed3-cb12-4544-88e0-4ba7cd68b88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "926ef915-d9fb-4503-9507-850ee8452960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f3fed3-cb12-4544-88e0-4ba7cd68b88d",
                    "LayerId": "3d336d3f-0b16-42b6-963a-0ba51fdf798f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3d336d3f-0b16-42b6-963a-0ba51fdf798f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}