{
    "id": "8303bf5d-fcb3-4426-8702-c8f79949337a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_floatings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b34a2415-e103-4ce6-b4c1-789090ea64ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8303bf5d-fcb3-4426-8702-c8f79949337a",
            "compositeImage": {
                "id": "01d87684-e109-40d8-955e-42d440ffdd50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34a2415-e103-4ce6-b4c1-789090ea64ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eddb5ffe-c17d-41ff-a69a-e646b7a115cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34a2415-e103-4ce6-b4c1-789090ea64ab",
                    "LayerId": "430b8606-89cd-448e-acbc-d8e04494d9e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "430b8606-89cd-448e-acbc-d8e04494d9e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8303bf5d-fcb3-4426-8702-c8f79949337a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 19
}