{
    "id": "dc52244a-8291-483d-83a2-55e873b83028",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_brokenshardr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "326f0d22-9728-4e4c-aab6-92fcf812159d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc52244a-8291-483d-83a2-55e873b83028",
            "compositeImage": {
                "id": "bb0728d6-9f7c-4b4c-a9b0-68e7b186a45c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "326f0d22-9728-4e4c-aab6-92fcf812159d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68a09ba-4b69-4405-965c-62e0637b7dd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "326f0d22-9728-4e4c-aab6-92fcf812159d",
                    "LayerId": "956c40f7-c1d7-42d4-a4ae-e63d9ce43509"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "956c40f7-c1d7-42d4-a4ae-e63d9ce43509",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc52244a-8291-483d-83a2-55e873b83028",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}