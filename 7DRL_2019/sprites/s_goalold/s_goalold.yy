{
    "id": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_goalold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94b34cea-d30e-4d08-a592-c287299f7562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "compositeImage": {
                "id": "e6be6cd8-3cda-42e1-9959-942fd0e91d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b34cea-d30e-4d08-a592-c287299f7562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "881b1a32-3c33-4319-84b3-b67cf7d7f41f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b34cea-d30e-4d08-a592-c287299f7562",
                    "LayerId": "4f85c1fd-a746-4b37-b15c-640005533fc1"
                }
            ]
        },
        {
            "id": "9f472fb2-5fdd-4ac3-92f9-3e4a45501a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "compositeImage": {
                "id": "b025cd67-e32a-4431-aace-1a045b0bff71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f472fb2-5fdd-4ac3-92f9-3e4a45501a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d402cd7f-9e8f-4a00-9eab-b36910831fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f472fb2-5fdd-4ac3-92f9-3e4a45501a75",
                    "LayerId": "4f85c1fd-a746-4b37-b15c-640005533fc1"
                }
            ]
        },
        {
            "id": "4d3d2a11-f350-437f-ae38-8944cdf91775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "compositeImage": {
                "id": "b163f175-fc4d-4393-a4d1-dd2e70dc190b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d3d2a11-f350-437f-ae38-8944cdf91775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1382a301-dd63-445b-afb3-32c824c6fdbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d3d2a11-f350-437f-ae38-8944cdf91775",
                    "LayerId": "4f85c1fd-a746-4b37-b15c-640005533fc1"
                }
            ]
        },
        {
            "id": "bcee303a-7e98-4ffa-a1ba-07585a8503ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "compositeImage": {
                "id": "4045430c-2f47-40e0-9d96-dacea8ad56c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcee303a-7e98-4ffa-a1ba-07585a8503ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4ff9db-f131-431f-90ee-eb488149e42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcee303a-7e98-4ffa-a1ba-07585a8503ae",
                    "LayerId": "4f85c1fd-a746-4b37-b15c-640005533fc1"
                }
            ]
        },
        {
            "id": "46941e3f-28f2-4667-b24b-1fe3ec852a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "compositeImage": {
                "id": "d628d7e5-d9a1-406b-a45f-626b97091ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46941e3f-28f2-4667-b24b-1fe3ec852a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00781d2-8fc3-45ed-8e0d-9d07c3df530b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46941e3f-28f2-4667-b24b-1fe3ec852a2d",
                    "LayerId": "4f85c1fd-a746-4b37-b15c-640005533fc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4f85c1fd-a746-4b37-b15c-640005533fc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8889c15c-bb7f-4cb6-aebf-46caa6f160aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 45
}