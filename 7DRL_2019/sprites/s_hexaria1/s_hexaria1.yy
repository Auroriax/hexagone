{
    "id": "9403498c-54f4-4d2c-9369-c9b374f41ae7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hexaria1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4560b4cf-1b27-4f93-b7ad-3f9a6872a425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9403498c-54f4-4d2c-9369-c9b374f41ae7",
            "compositeImage": {
                "id": "e9fe060a-cab2-4535-a7fb-f6ff30bf6af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4560b4cf-1b27-4f93-b7ad-3f9a6872a425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e98600-1bbb-4f98-9b79-cdddb0981da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4560b4cf-1b27-4f93-b7ad-3f9a6872a425",
                    "LayerId": "663d9b28-1471-4ca6-a148-d97bee9f6a96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "663d9b28-1471-4ca6-a148-d97bee9f6a96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9403498c-54f4-4d2c-9369-c9b374f41ae7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}