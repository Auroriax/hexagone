//blowup(all?)

if argument0 == false
{ //Just one
	var b = instance_create_depth(x,y,depth-200,o_blownup)
	b.sprite_index = sprite_index; b.image_index = image_index;
	b.direction = random_range(0,180)
	b.speed = 5;
	b.gravity = 0.2;
	b.anginc = random_range(-2,2)
	b.alarm[11] = 1;	
}
else
with(o_tile) //ALL OF THEM!
{
	var b = instance_create_depth(x,y,depth-200,o_blownup)
	b.sprite_index = sprite_index; b.image_index = image_index;
	b.direction = random_range(0,180)
	b.speed = 5;
	b.gravity = 0.2;
	b.anginc = random_range(-2,2)
	b.alarm[11] = 1;	
}