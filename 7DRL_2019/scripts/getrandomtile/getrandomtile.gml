//getrandomtile(type)
//Returns array of two deep.

matchfound = false;

with o_generator
{
	while(!matchfound)
	{
		var rand;
		rand[0] = irandom(g_width);
		rand[1] = irandom(g_height)
		
		if argument_count == 0
		{
			if tile[rand[0],rand[1]] != FORBIDDEN
			{
				return(rand)
			}
		}
		else
		{
			if tile[rand[0],rand[1]] == argument0
			{
				return(rand)
			}
		}
	}
}