//moveindir(TOP|TOPLEFT|BOTTOMLEFT|BOTTOM|BOTTOMRIGHT|TOPRIGHT, mod)

var modi = 1;
if argument_count >= 2 {modi = argument[1];}

switch (argument[0])
{
case 0: y -= (tileheight*2)*modi; break;
case 1: x += (tilewidth*2)*modi; y -= tileheight*modi; break;
case 2: x += (tilewidth*2)*modi; y += tileheight*modi; break;
case 3: y += (tileheight*2)*modi; break;
case 4: x -= (tilewidth*2)*modi; y += tileheight*modi; break;
case 5: x -= (tilewidth*2)*modi; y -= tileheight*modi; break;
}

return(true);