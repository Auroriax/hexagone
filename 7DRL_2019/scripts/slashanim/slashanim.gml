///slashanim(x,y,depth,clr)

var s = instance_create_depth(argument0,argument1,argument2,o_slash)
s.image_blend = argument3

if argument3 == c_red
sound(a_hit,random_range(0.6,0.7),0.6)
else
sound(a_hit,random_range(1,1.1),0.6)