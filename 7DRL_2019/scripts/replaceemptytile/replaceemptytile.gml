//countnearbytiles(x,y,object_id)
//Always run this script assuming there's a free tile

var rng = irandom(5)
var tile = noone;

while (tile == noone)
{
	switch (rng)
	{
		case 0: tile = instance_place(argument0,argument1 - tileheight*2,argument2); break;
		case 1: tile =  instance_place(argument0 + tilewidth*2 ,argument1 - tileheight,argument2); break;
		case 2: tile =  instance_place(argument0 + tilewidth*2 ,argument1 + tileheight,argument2); break;
		case 3: tile =  instance_place(argument0,argument1 + tileheight*2,argument2); break;
		case 4: tile =  instance_place(argument0 - tilewidth*2 ,argument1 + tileheight,argument2); break;
		case 5: tile =  instance_place(argument0 - tilewidth*2 ,argument1 - tileheight,argument2); break;
	}
	rng += 1; if rng > 6 {rng -= 6}
}

with tile
{
	var new = instance_create_depth(x,y,depth,o_tile)
	new.ii = ii; new.jj = jj;
	instance_destroy();
}