//countnearbytiles(x,y,object_id)

var cnt = 0;

cnt += place_meeting(argument0,argument1 - tileheight*2,argument2)
cnt += place_meeting(argument0 + tilewidth*2 ,argument1 - tileheight,argument2)
cnt += place_meeting(argument0 + tilewidth*2 ,argument1 + tileheight,argument2)
cnt += place_meeting(argument0,argument1 + tileheight*2,argument2)
cnt += place_meeting(argument0 - tilewidth*2 ,argument1 + tileheight,argument2)
cnt += place_meeting(argument0 - tilewidth*2 ,argument1 - tileheight,argument2)

return(cnt);