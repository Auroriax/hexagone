///levelup()

with o_glyph_parent {instance_destroy()};

o_generator.enemiestodo = floor(o_generator.enemies) + (o_player.goaltiles * o_generator.enemiesup);

if o_player.goaltiles >= 2
o_generator.fragiletodo = floor(o_generator.fragile) + (o_player.goaltiles * o_generator.fragileup);
else
o_generator.fragiletodo = 0;

o_generator.fillintodo = floor(o_generator.fillin) + (o_player.goaltiles * o_generator.fillinup);
o_generator.alarm[0] = 1;

sound(a_level,1)

random_set_seed(cachedseed);