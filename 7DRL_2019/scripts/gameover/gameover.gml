///gameover(takescreenshot)

if winsprite != noone {sprite_delete(winsprite)}

if argument_count == 0
{
	winsprite = sprite_create_from_surface(application_surface,0,0,view_wport[0],view_hport[0],false,false,0,0)
}

//Highscores
if o_player.steps >= highsteps
{highsteps = o_player.steps}

if o_player.enemiesdefeated >= highdefeated
{highdefeated = o_player.enemiesdefeated}

if o_player.goaltiles >= highlevels
{highlevels = o_player.goaltiles}

//History of runs
for (var i = 0; i != 9; i += 1)
{
	paststeps[i] = paststeps[i+1] 
	pastdefeated[i] = pastdefeated[i+1] 
	pastlevels[i] = pastlevels[i+1] 
}
paststeps[9] = o_player.steps
pastdefeated[9] = o_player.enemiesdefeated
pastlevels[9] = o_player.goaltiles

room_goto(graphs);