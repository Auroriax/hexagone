//tutorial, always call from o_player

with o_glyph_parent {instance_destroy()}

var glyph = instance_create_depth(x,y,depth,o_glyph_parent)
glyph.dir = dir;

glyph = instance_create_depth(x,y,depth,o_glyph_down)
glyph.dir = (dir+6 + 3) % 6

glyph = instance_create_depth(x,y,depth,o_glyph_left)
glyph.dir = (dir+6 -1) % 6
glyph.off = 0.5

glyph = instance_create_depth(x,y,depth,o_glyph_right)
glyph.dir = (dir+6 + 1) % 6
glyph.off = 0.5