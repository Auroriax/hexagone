///playermoved(dir,playerturn?)

movedintodir = argument[0];
var onplayerturn = true;
if argument_count >= 2 {onplayerturn = argument[1]}

switch(checktile(x,y))
{
	case o_tile: case o_tile_fragile: case o_goal: break;
	default: 
	if onplayerturn
	{
		instance_create_depth(x,y,-1000,o_cross) 
		movetodir((movedintodir+6 -3) % 6); 
		onplayerturn = false; 
	}
	else
	{
		o_player.alarm[11] = 7; return(false)
	}
	break; //Undo

}

while(checkenemy(x,y) != noone) //Push all enemies
{
	camshake(20);
	var enemy = checkenemy(x,y);
	with enemy
	{	stunned = true;
		movetodir(other.movedintodir)
		pushed = other.movedintodir;
		if abs(dir - other.movedintodir) == 0 {movetodir(other.movedintodir); pushed = true;} //Backstabs do double damage
		slashanim(x,y,other.depth,c_white)
		
		if object_index == o_enemy_sniper
		{dir = (other.dir + 6 -3) % 6}
		
		if object_index = o_enemy_slingshot
		{
			countdown += 1;	
		}
	}
}

return(onplayerturn)