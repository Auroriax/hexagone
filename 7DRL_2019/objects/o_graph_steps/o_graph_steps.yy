{
    "id": "cec30cb8-da4f-418d-a69b-797b4d88a7b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_graph_steps",
    "eventList": [
        {
            "id": "f6795c29-d068-4c42-9b2a-8e07b1adca83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cec30cb8-da4f-418d-a69b-797b4d88a7b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ac917a5-c103-4e1a-9946-b0cad03a53e3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
    "visible": true
}