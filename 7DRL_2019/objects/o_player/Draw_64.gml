/// @description Draw variables

if (!drawui) {exit;}

var xoff = (room_width/2)+200;
var height = view_hport[0]-40;
var textoff = 16;
draw_set_halign(0); draw_set_valign(1)

draw_sprite(s_ui,0,xoff+10,height);
draw_text(xoff+50,height+textoff,string(steps));

draw_sprite(s_ui,1,xoff+100,height);
draw_text(xoff+140,height+textoff,string(enemiesdefeated));

draw_sprite(s_ui,2,xoff+180,height);
draw_text(xoff+220,height+textoff,string(goaltiles))