/// @description Tank controls

if o_generator.alarm[0] != -1 || o_generator.alarm[1] || o_generator.alarm[2] || o_generator.alarm[3] {exit;}

if alarm[0] != -1 {exit}

if countnearbytiles(x,y,o_tile_parent) == 0
{
	if alarm[11] == -1 {alarm[11] = 2;}
	exit;
}

if keyboard_check_pressed(vk_left) || gamepad_button_check_pressed(gpid,gp_padl)  || gamepad_button_check_pressed(gpid,gp_shoulderl)
{
	image_angle += 60;	
	dir -= 1;
	if dir == -1 {dir = 5}
	sound(a_step,random_range(0.6,0.7))
	
		if goaltiles == 0
		{
			tutorial();	
		}
}
else
if keyboard_check_pressed(vk_right) || gamepad_button_check_pressed(gpid,gp_padr)  || gamepad_button_check_pressed(gpid,gp_shoulderr)
{
	image_angle -= 60;	
	dir += 1;
	if dir == 6 {dir = 0}
	sound(a_step,random_range(0.6,0.7))
	
		if goaltiles == 0
		{
			tutorial();	
		}
}
else
if keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(gpid,gp_padu) || gamepad_button_check_pressed(gpid,gp_face1)
{
	movetodir(dir)
	
	if playermoved(dir) {
		alarm[0] = 6;
		steps += 1;
	}
	
	sound(a_step,random_range(0.9,1.1))
	
	if goaltiles == 0
		{
			tutorial();	
		}
}
else
if keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(gpid,gp_padd) || gamepad_button_check_pressed(gpid,gp_face2)
{
	movetodir((dir + 6 -3) % 6)
	
	if playermoved((dir + 6 -3) % 6) {
		alarm[0] = 6
		steps += 1;
	}
	
		sound(a_step,random_range(0.9,1.1))
		
		if goaltiles == 0
		{
			tutorial();	
		}
}