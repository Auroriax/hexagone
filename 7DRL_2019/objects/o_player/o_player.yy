{
    "id": "0645bf4c-bca7-4413-8fe6-80fa805f82de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "2ae922c9-d99d-47d3-8ecf-d98543cb4eaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        },
        {
            "id": "58beb3b8-30a0-4933-8828-83eaa7500579",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        },
        {
            "id": "628676fe-af40-4620-ad6e-12f326b1e662",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        },
        {
            "id": "6bdbae2e-042c-437c-ac2c-17248c223192",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        },
        {
            "id": "bc480cd0-631c-4eb6-8c80-535eb70ae7aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        },
        {
            "id": "6e07108f-2f03-4d82-9f59-9357eb7940f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "0645bf4c-bca7-4413-8fe6-80fa805f82de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6aa846b1-e70d-48dd-ab3e-538a577d0a82",
    "visible": true
}