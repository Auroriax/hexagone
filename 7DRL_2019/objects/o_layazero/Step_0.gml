/// @description QQQ

if keyboard_check(vk_escape)
{
	if close < 0 {exit}
	close += 0.010;
	if close >= 1
	{
		io_clear();
		close = -1
		if room == generator
		{
			gameover(false);
		}
		else
		{
			if os_browser == browser_not_a_browser
			{
				game_end();
			}
			else
			{
				audio_stop_all();
				game_restart();
			}
		}
	}
}
else
{
	if close < 0 {close = 0}
	if close >= 0 {close -= 0.2; if close < 0 {close = 0;}}
}