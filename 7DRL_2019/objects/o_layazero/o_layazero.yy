{
    "id": "22e878a6-8c34-450b-b6bc-594b4e3b70f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_layazero",
    "eventList": [
        {
            "id": "e901e9d3-0960-49fb-9957-0b91c1ea2133",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "61773d97-089b-4cb0-ac73-83482b816ff0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "eea0dd67-9c45-47ff-9a43-0a0560e831a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 116,
            "eventtype": 9,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "275f30ee-8a4e-4d79-9391-dc20cf285b95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "c4cc5566-8937-4759-ab29-9978f86de0e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "9d982384-8986-4a66-883c-3042e4fadd9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 9,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "2a9b52df-5a93-4a1b-a57a-39f14c8a8a12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 118,
            "eventtype": 9,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        },
        {
            "id": "834c881a-629c-4022-9560-36120560f7d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 119,
            "eventtype": 9,
            "m_owner": "22e878a6-8c34-450b-b6bc-594b4e3b70f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}