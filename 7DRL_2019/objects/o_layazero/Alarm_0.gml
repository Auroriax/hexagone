/// @description Check controller

//Debug
if keyboard_lastchar == vk_f12
window_set_caption(string(fps) + " " + string(instance_count) + " " + date_datetime_string(GM_build_date))
else
window_set_caption("⬣ - 7drl 2019 - Tom Hermans (@Auroriax)")

alarm[0] = 60;

if gamepad_is_supported()
{
	for( var i = 0; i != 12; i += 1)
	{
		if gamepad_is_connected(i)	
		{
			gpid = i; break;
		}
	}
}