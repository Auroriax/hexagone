/// @description Music

if audio
{
	audio_master_gain(0)
	audio = false
}
else
{
	audio_master_gain(1)
	audio = true
}