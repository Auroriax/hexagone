/// @description It's unclear whether this holographic planet is real or an illusion. Layazeroes draw strength from holographic designs.

globalvar TOP; TOP = 0;
globalvar TOPRIGHT; TOPRIGHT = 1;
globalvar BOTTOMRIGHT; BOTTOMRIGHT= 2;
globalvar BOTTOM; BOTTOM = 3;
globalvar BOTTOMLEFT; BOTTOMLEFT = 4;
globalvar TOPLEFT; TOPLEFT = 5;

alarm[0] = 60;

randomize();
globalvar cachedseed; cachedseed = random_get_seed();

draw_set_font(SourceCodePro)

globalvar gpid; gpid = 0;

keyboard_set_map(ord("A"), vk_left)
keyboard_set_map(ord("W"), vk_up)
keyboard_set_map(ord("S"), vk_down)
keyboard_set_map(ord("D"), vk_right)
keyboard_set_map(ord("Z"), vk_up)
keyboard_set_map(ord("Q"), vk_left)

globalvar paststeps; globalvar pastdefeated; globalvar pastlevels;
globalvar highsteps; globalvar highdefeated; globalvar highlevels;
highsteps = 0; highdefeated = 0; highlevels = 0;

for (var i = 0; i != 10; i += 1)
{
 paststeps[i] = -1;
 pastdefeated[i] = -1;
 pastlevels[i] = -1;
}

globalvar winsprite; winsprite = noone;

audio_play_sound(a_downtime,0,true);
//invincible = false;
//checkpoint = noone;

close = 0;
audio = true;

gpu_set_alphatestenable(true);

if os_browser != browser_not_a_browser
display_set_gui_maximize();

room_goto_next();