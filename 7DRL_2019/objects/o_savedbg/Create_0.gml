/// @description Show winsprite

if winsprite!= noone && sprite_exists(winsprite)
sprite_index = winsprite;

image_xscale = 0.5; image_yscale = 0.5;

sound(a_level,0.5)