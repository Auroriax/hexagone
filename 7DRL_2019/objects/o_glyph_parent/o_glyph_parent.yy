{
    "id": "e085dcfc-ed44-4851-a081-086974e00b41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_glyph_parent",
    "eventList": [
        {
            "id": "b455e235-70b1-4bf5-8909-d58fd1817f56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e085dcfc-ed44-4851-a081-086974e00b41"
        },
        {
            "id": "edf3fee1-e45f-4fbd-9aa5-e9db9c8ab4b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e085dcfc-ed44-4851-a081-086974e00b41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5fee020a-afaf-459a-b554-fe8c3000d2b3",
    "visible": true
}