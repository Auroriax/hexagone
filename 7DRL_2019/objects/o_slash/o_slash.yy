{
    "id": "cc4e4bb8-cdfd-4eab-a9ab-d243f07ce47e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slash",
    "eventList": [
        {
            "id": "305cceda-d683-4459-be33-bd5cc4b2b3eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc4e4bb8-cdfd-4eab-a9ab-d243f07ce47e"
        },
        {
            "id": "b3c2f1c7-b1e1-4007-83a9-08d49b953ecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "cc4e4bb8-cdfd-4eab-a9ab-d243f07ce47e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34e84d98-0ed9-4136-acf5-966bba8d57bc",
    "visible": true
}