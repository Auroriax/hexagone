/// @description Move goal

var furthestaway = -1;
var furthestdist = -1;

for (var i = 0; i != 10; i += 1)
{
	var nr = irandom(instance_number(o_spaceparent)-1)
	var randtile = instance_find(o_spaceparent,nr)

	var dist = point_distance(randtile.x,randtile.y,o_player.x,o_player.y)
	if dist > furthestdist
	{
		furthestaway = randtile;
		furthestdist = dist;
	}
}

if furthestaway != noone
{
	with o_goal //Destroy old goal
	{
		var new = instance_create_depth(x,y,depth,o_tile)
		new.ii = ii; new.jj = jj;
		instance_destroy(self);
	}
	with furthestaway //Make new goal
	{
		var new = instance_create_depth(x,y,depth,o_goal)
		new.ii = ii; new.jj = jj;
		
		with (new)
		{
			while (countnearbytiles(x,y,o_tile_parent) < 3)
			{
				//show_debug_message("Replacing empty tiles... "+string(countnearbytiles(x,y,o_tile_parent)))
				replaceemptytile(x,y,o_empty)
			}	
		}
		instance_destroy(self);
	}
	show_debug_message("Created new goal at a distance of "+string(furthestdist))

}
else
{
	show_debug_message("ERROR: Could not move goal!")	
}

cachedseed = random_get_seed();

//if buffer_exists(checkpoint)
//buffer_delete(checkpoint)

//game_save_buffer(checkpoint)