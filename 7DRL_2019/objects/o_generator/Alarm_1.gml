/// @description Empty -> Complete

if fillintodo > 0
{
	fillintodo -= 1;
	
	if (instance_number(o_empty) != 0)
	{
		var nr = irandom(instance_number(o_empty)-1)
		var randtile = instance_find(o_empty,nr)

		if randtile != noone
		{
			with randtile
			{
				var new = instance_create_depth(x,y,depth,o_tile)
				new.ii = ii; new.jj = jj;
				instance_destroy(self);
			}
		}
	}
	
	alarm[1] = 1
}
else
{
	alarm[2] = 20;	
}