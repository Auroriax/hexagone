/// @description Add enemies

if enemiestodo > 0
{
	enemiestodo -= 1;
	var nr = irandom(instance_number(o_tile_parent)-1)
	var randtile = instance_find(o_tile_parent,nr)

	if randtile != noone
	{
		with randtile
		{
			if !place_meeting(x,y,o_enemy_parent) && !place_meeting(x,y,o_player)
			{//Only place a new enemy if the space is currently not occupied.
				var en = o_enemy_copy //choose(o_enemy_copy,o_enemy_slingshot)
				if o_player.goaltiles == 3 {en = o_enemy_slingshot}
				else if o_player.goaltiles == 4 || o_player.goaltiles == 5 {en = choose(o_enemy_copy,o_enemy_slingshot)}
				else if o_player.goaltiles == 6  {en = o_enemy_sniper}
				else if o_player.goaltiles >= 7 {en = choose(o_enemy_copy,o_enemy_slingshot,o_enemy_sniper)}
				var new = instance_create_depth(x,y,-700,en)
			}
		}
	}
	
	alarm[2] = 1
}
else
{
	alarm[3] = 10;	
}