/// @description Complete -> Fragile

if fragiletodo > 0
{
	fragiletodo -= 1;
	
	if (instance_number(o_tile) != 0)
	{
		var nr = irandom(instance_number(o_tile)-1)
		var randtile = instance_find(o_tile,nr)

		show_debug_message("Picking "+string(randtile)+" out of "+string(nr))

		if randtile != noone
		{
			with randtile
			{
				var new = instance_create_depth(x,y,depth,o_tile_fragile)
				new.ii = ii; new.jj = jj; new.yoff += 400;
				blowup(false);
				instance_destroy();
			}
		}
	}
	
	alarm[0] = 1
}
else
{
	alarm[1] = 20;	
}