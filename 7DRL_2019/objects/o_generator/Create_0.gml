/// @description Generate world

//Create empty level
globalvar g_width; g_width = 11; 
globalvar g_height; g_height = 10;
botleftcorner = 4 //9-5;
toprightcorner = 5 //9-5;

#macro FORBIDDEN -2

for(var i = 0; i != g_width; i += 1)
{
	for(var j = 0; j != g_height; j += 1)
	{
		var sum = i + j
		if (j > botleftcorner + i || (i > toprightcorner && j < i - toprightcorner) )
		tile[i,j] = FORBIDDEN;
		else
		tile[i,j] = o_tile;
	}
}

//Generate tiles in those locations

globalvar tilewidth; tilewidth =  24;
globalvar tileheight; tileheight = 15;

for(var i = 0; i != g_width; i += 1)
{
	for(var j = 0; j != g_height; j += 1)
	{
		if tile[i,j] != FORBIDDEN
		{
			ins = instance_create_depth(x+hx(i), y+hy(j,i), (-j*20)+i-20, tile[i,j]);
			
			ins.ii = i; ins.jj = j;
		}
	}
}

fragiletodo = -1;
fillintodo = -1;
enemiestodo = -1;
alarm[0] = 5; //Place enemies & place goal tile

playeri = floor(g_width/2)
playerj = floor(g_height/2)
show_debug_message(string(playeri)+" "+string(playerj))
player = instance_create_depth(x+hx(playeri),y+hy(playerj,playeri),-700,o_player)

//Level up stats
fragile = 8; //Replace full tiles by fragile ones
fillin = 1; //Replace empty tiles by full ones
enemies = 2 //2;

fragileup = +0.5; 
fillinup = +1;
enemiesup = 0.25;