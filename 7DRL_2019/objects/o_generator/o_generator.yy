{
    "id": "3f5b2aa0-1181-432a-a177-9d1f2fa68156",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_generator",
    "eventList": [
        {
            "id": "d1941d3f-53e9-4f56-8796-084a34715724",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        },
        {
            "id": "1c5494f4-12eb-4b0b-958f-0d18d5f34d21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        },
        {
            "id": "61136186-9d37-44af-a806-2b92d1ce4b0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        },
        {
            "id": "d4559dde-03de-40a6-8df4-3b7162b0a64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        },
        {
            "id": "4383c5e6-35dc-4897-96c1-43100bbfa70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        },
        {
            "id": "21eff3c0-45a5-476b-b725-e70872275c19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "3f5b2aa0-1181-432a-a177-9d1f2fa68156"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
    "visible": true
}