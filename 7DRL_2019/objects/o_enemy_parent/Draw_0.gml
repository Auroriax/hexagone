/// @description Draw thing + countdown

draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,-1,1)

if countdown >= 1 && countdown <= 5
{
	var clr = c_red;
	txtoutline(x+12,y+5,string(countdown),c_black)
	draw_text_color(x+12,y+5,string(countdown),clr,clr,clr,clr,1)
}
