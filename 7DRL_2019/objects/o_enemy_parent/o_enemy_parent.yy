{
    "id": "5a6e46f8-4461-412d-9aef-63e459885239",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_parent",
    "eventList": [
        {
            "id": "1e34b766-38c2-480a-9d3a-7d85c1015d80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a6e46f8-4461-412d-9aef-63e459885239"
        },
        {
            "id": "92023bc8-db41-4c5f-9060-0451963f852c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5a6e46f8-4461-412d-9aef-63e459885239"
        },
        {
            "id": "6f4a7cc9-b60a-4e04-8bd3-d8c459ed1367",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a6e46f8-4461-412d-9aef-63e459885239"
        },
        {
            "id": "2de65b3c-5854-42f3-b5f7-12a858b6381e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a6e46f8-4461-412d-9aef-63e459885239"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}