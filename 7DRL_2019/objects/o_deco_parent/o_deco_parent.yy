{
    "id": "5869e097-189b-4761-8450-7502966deef8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_deco_parent",
    "eventList": [
        {
            "id": "7ff3e883-83ca-4989-a6f9-046ebe354e59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5869e097-189b-4761-8450-7502966deef8"
        },
        {
            "id": "42db3449-c578-4072-8189-7a1b8c4a1eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5869e097-189b-4761-8450-7502966deef8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8303bf5d-fcb3-4426-8702-c8f79949337a",
    "visible": true
}