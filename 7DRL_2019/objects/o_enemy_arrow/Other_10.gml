/// @description ACT

countdown -= 1;
if countdown <= 0 {instance_destroy(); exit;}

if stunned {stunned = false; exit;}

movetodir(dir);

if place_meeting(x,y,o_player)
{
	with (o_player)
	{
		camshake(15);
		movetodir(other.dir); //Push
		playermoved(other.dir,false);
		
		movetodir(other.dir); //Push
		playermoved(other.dir,false);
		slashanim(x,y,depth,c_red)
	}
	instance_destroy();
}

/*if place_meeting(x,y,o_enemy_arrow)
{
	with instance_place(x,y,o_enemy_arrow)
	{
		instance_destroy()	
	}
	
	instance_destroy();
}