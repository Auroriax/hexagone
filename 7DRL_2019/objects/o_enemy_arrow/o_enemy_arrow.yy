{
    "id": "bc2be96f-a536-4f31-a573-e66e70b47b45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_arrow",
    "eventList": [
        {
            "id": "12ffbcdb-80bc-4a1f-8c2a-fbdffc2fe7c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "bc2be96f-a536-4f31-a573-e66e70b47b45"
        },
        {
            "id": "f5a5c4e1-6e93-47a9-8ae7-ac630fada47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bc2be96f-a536-4f31-a573-e66e70b47b45"
        },
        {
            "id": "5c5cfbea-a62d-435c-bf55-eeb655f14f93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc2be96f-a536-4f31-a573-e66e70b47b45"
        },
        {
            "id": "43388468-9ac7-4462-9ae1-045180605cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bc2be96f-a536-4f31-a573-e66e70b47b45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ee675dc-71f0-4d8f-989f-9c5cedfba160",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf812ee9-3bc5-499d-9758-cda4dd2a6eec",
    "visible": true
}