/// @description Set collapse state

if yoff > 5 {exit;}

var supportingweight = (place_meeting(x,y,o_player) || (place_meeting(x,y,o_enemy_parent && !place_meeting(x,y,o_enemy_arrow))))

if (supportingweight) && !fragile
{
	fragile = true;
	alarm[2] = 30;
	visible = false
}

if !(supportingweight) && fragile
{
	//o_generator.tile[ii,jj] = o_empty
	var empty = instance_create_depth(x,y,depth,o_empty)
	empty.ii = ii; empty.jj = jj;
	instance_destroy();	
}