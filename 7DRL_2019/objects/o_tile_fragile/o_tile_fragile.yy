{
    "id": "c64df022-88c6-4298-bfed-cb18053b9605",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tile_fragile",
    "eventList": [
        {
            "id": "5e8d66e5-514f-4b30-ae87-8a0ad769ced3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c64df022-88c6-4298-bfed-cb18053b9605"
        },
        {
            "id": "b4cc2ca2-1c23-4acc-a81d-90fee6ea69e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c64df022-88c6-4298-bfed-cb18053b9605"
        },
        {
            "id": "ce7ebad2-17fb-43e3-9374-2effad79f0d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c64df022-88c6-4298-bfed-cb18053b9605"
        },
        {
            "id": "65176ca6-8767-470c-87a9-e4c539c0800d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c64df022-88c6-4298-bfed-cb18053b9605"
        }
    ],
    "maskSpriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
    "overriddenProperties": null,
    "parentObjectId": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d421789f-807c-4e55-9836-9fefab3b3868",
    "visible": true
}