{
    "id": "c9a4ea44-12cc-42eb-a00b-0237f63b6a49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_empty",
    "eventList": [
        {
            "id": "ec2a19c0-3127-4c61-b53a-a115b90f1e2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9a4ea44-12cc-42eb-a00b-0237f63b6a49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a1326931-aeac-4a3d-84f9-52c417bbd320",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5821dc6b-bc21-4636-9897-2062903784a1",
    "visible": true
}