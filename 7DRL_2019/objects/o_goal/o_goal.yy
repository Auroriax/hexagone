{
    "id": "9a1598ff-9778-40cd-a13d-9bbb76cf0279",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_goal",
    "eventList": [
        {
            "id": "134adeda-939b-473b-8461-db2a461d5aff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a1598ff-9778-40cd-a13d-9bbb76cf0279"
        },
        {
            "id": "564303d4-2469-4ee6-9569-a5878ec5c0cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a1598ff-9778-40cd-a13d-9bbb76cf0279"
        },
        {
            "id": "6bc1cfc7-6425-4d32-bd5c-39b9cd297885",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9a1598ff-9778-40cd-a13d-9bbb76cf0279"
        },
        {
            "id": "3347a228-6eb9-48ac-824c-0b023641215e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9a1598ff-9778-40cd-a13d-9bbb76cf0279"
        }
    ],
    "maskSpriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
    "overriddenProperties": null,
    "parentObjectId": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6b8ab33-a632-4f6a-b955-652c43f7e4c2",
    "visible": true
}