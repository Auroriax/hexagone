/// @description QQQ

event_inherited();

if !activated
{
	goalanim += 0.75;
	if goalanim > sprite_get_number(s_goalanim) 
		{goalanim -= sprite_get_number(s_goalanim)}
	draw_sprite(s_goalanim,goalanim,x,y+yoff)
}