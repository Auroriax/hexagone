/// @description Check if player is on it

if place_meeting(x,y,o_player) && !activated
{
	o_player.goaltiles += 1;
	activated = true;
	levelup();
}