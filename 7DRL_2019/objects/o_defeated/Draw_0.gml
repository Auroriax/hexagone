/// @description Draw taking direction into account

if sprite_exists(sprite_index)
draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,-1,image_alpha)