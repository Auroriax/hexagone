{
    "id": "02349ffb-28d7-4894-90f7-a702be92e9a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_defeated",
    "eventList": [
        {
            "id": "4b0bd860-0f79-4fb6-ad5a-0d7dd489717c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02349ffb-28d7-4894-90f7-a702be92e9a3"
        },
        {
            "id": "93a8cb42-85bd-46a7-b163-e22e51b374b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02349ffb-28d7-4894-90f7-a702be92e9a3"
        },
        {
            "id": "9c0cbcef-3efe-48f7-8ccd-8434a987943c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "02349ffb-28d7-4894-90f7-a702be92e9a3"
        },
        {
            "id": "70a8ae0e-be8e-4ff7-831a-60473f65d259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "02349ffb-28d7-4894-90f7-a702be92e9a3"
        },
        {
            "id": "7252bc9f-e8f0-4586-9b00-0bcc05545d33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "02349ffb-28d7-4894-90f7-a702be92e9a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}