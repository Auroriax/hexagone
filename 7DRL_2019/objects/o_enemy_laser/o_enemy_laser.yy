{
    "id": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_laser",
    "eventList": [
        {
            "id": "08116eb0-d45e-4b7e-bec2-5c6157e69dd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb"
        },
        {
            "id": "c1c952fe-9589-4f30-8277-a4b35bdafe5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb"
        },
        {
            "id": "a9768b86-9545-4945-bcd1-6abfede8462d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb"
        },
        {
            "id": "ed136aa5-c4e8-40c1-a45d-11c7b7df4bdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb"
        },
        {
            "id": "cec56a39-a638-405d-9e6b-1f3ea50fe5d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "186bb469-e8a2-41f2-9cf4-79004a0cb8bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5a6e46f8-4461-412d-9aef-63e459885239",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c32b9e1-2391-495a-abca-5d8cb6dc23fd",
    "visible": true
}