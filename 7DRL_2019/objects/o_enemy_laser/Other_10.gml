/// @description ACT

if shot { instance_destroy();}

var startx = x; var starty = y;

while ( x > 0 &&  x < room_width && y > 0 && y < room_height)
{
	movetodir(dir)
	
	if instance_place(x,y,o_player)
	{
		with (o_player)
		{
			camshake(15);
			movetodir(other.dir); //Push
			playermoved(other.dir,false);
			slashanim(x,y,depth,c_red)
		
			movetodir(other.dir); //Push
			playermoved(other.dir,false);
			slashanim(x,y,depth,c_red)
		
			movetodir(other.dir); //Push
			playermoved(other.dir,false);
			slashanim(x,y,depth,c_red)
		}
		break;
	}
}

x = startx; y = starty;
shot = true;