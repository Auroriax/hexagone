{
    "id": "e910480f-e14d-4b67-80f5-293b748c4dbf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tile",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbe3c5bb-171e-4367-9594-bb9c88cf47a4",
    "visible": true
}