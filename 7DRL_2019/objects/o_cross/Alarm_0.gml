/// @description Decrease alpha

image_alpha -= 0.10;

if image_alpha <= 0
{
instance_destroy();
}

alarm[0] = 1;