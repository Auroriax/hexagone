{
    "id": "57828939-e7e6-47f7-961f-c7cfabfc1907",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cross",
    "eventList": [
        {
            "id": "bee15651-4cc2-4782-8aef-616ce1ddca6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57828939-e7e6-47f7-961f-c7cfabfc1907"
        },
        {
            "id": "cf51897c-f75f-45c1-8861-e4ec1856f093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "57828939-e7e6-47f7-961f-c7cfabfc1907"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f09f1094-fe1d-4c08-a6d3-085abad074b1",
    "visible": true
}