{
    "id": "d381c840-ad53-4305-8ce4-29fc1ce5fba0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_slingshot",
    "eventList": [
        {
            "id": "66448d4a-b888-489d-9743-f4d2f10d53d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d381c840-ad53-4305-8ce4-29fc1ce5fba0"
        },
        {
            "id": "6fb26cc9-4e48-4078-9cda-03b8196c30a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d381c840-ad53-4305-8ce4-29fc1ce5fba0"
        },
        {
            "id": "1941466f-41ac-4f79-896f-3dcb0702e4bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d381c840-ad53-4305-8ce4-29fc1ce5fba0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ee675dc-71f0-4d8f-989f-9c5cedfba160",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "efbf0dd1-5896-4bbd-8ea5-d71432a06ca1",
    "visible": true
}