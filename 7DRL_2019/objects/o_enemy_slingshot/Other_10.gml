/// @description ACT

if countdown != 0 {countdown -= 1;}

if !place_meeting(x,y,o_tile_parent) {instance_destroy(); exit;}

//if stunned {stunned = false; exit;}

var prec = false;

if (collision_line(x,y,x,y-(g_height*tileheight)*2,o_player,prec,true) != noone && (dir != BOTTOM)) 
{dir = TOP;}
else if (collision_line(x,y,x-(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != BOTTOMRIGHT) 
{dir = TOPLEFT;}
else if (collision_line(x,y,x-(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != TOPRIGHT) 
{dir = BOTTOMLEFT;}
else if (collision_line(x,y,x,y+(g_height*tileheight)*2,o_player,prec,true) != noone && dir != TOP) 
{dir = BOTTOM;}
else if (collision_line(x,y,x+(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != TOPLEFT) 
{dir = BOTTOMRIGHT;}
else if (collision_line(x,y,x+(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != BOTTOMLEFT) 
{dir = TOPRIGHT;}
else {exit}

if countdown == 0
{
	var arrow = instance_create_depth(x,y,o_player.depth,o_enemy_arrow)
	arrow.dir = dir;
	//arrow.stunned = true;
	self.countdown = maxcooldown;
}

//movetodir(dir);

