/// @description ACT

if !place_meeting(x,y,o_tile_parent) {instance_destroy(); exit;}

if stunned {stunned = false; exit;}

var prec = false;

var newdir = -1;

if (collision_line(x,y,x,y-(g_height*tileheight)*2,o_player,prec,true) != noone && (dir != BOTTOM)) 
{newdir = TOP;}
else if (collision_line(x,y,x-(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != BOTTOMRIGHT) 
{newdir = TOPLEFT;}
else if (collision_line(x,y,x-(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != TOPRIGHT) 
{newdir = BOTTOMLEFT;}
else if (collision_line(x,y,x,y+(g_height*tileheight)*2,o_player,prec,true) != noone && dir != TOP) 
{newdir = BOTTOM;}
else if (collision_line(x,y,x+(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != TOPLEFT) 
{newdir = BOTTOMRIGHT;}
else if (collision_line(x,y,x+(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir != BOTTOMLEFT) 
{newdir = TOPRIGHT;}
else {exit}

if (newdir == dir || abs(newdir - dir) == 1 || abs(newdir - dir) == 5 )
{
	dir = newdir;
	movetodir(dir);

	if !place_meeting(x,y,o_tile_parent) || place_meeting(x,y,o_enemy_parent)
	{
		movetodir((dir + 6 -3) % 6); exit;
	}

	if place_meeting(x,y,o_player)
	{
		with (o_player)
		{
			camshake(15);
			movetodir(other.dir); //Push
			playermoved(other.dir,false);
			slashanim(x,y,depth,c_red)
		}
	}
}
else
{
	dir = newdir;
}