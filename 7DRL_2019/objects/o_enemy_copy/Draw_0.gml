/// @description Debug

event_inherited()

if keyboard_lastkey == vk_f10
{
draw_line(x,y,x,y-(g_height*tileheight)*2);
draw_line(x,y,x+(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2);
draw_line(x,y,x+(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2);
draw_line(x,y,x,y+(g_height*tileheight)*2);
draw_line(x,y,x-(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2);
draw_line(x,y,x-(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2);
}