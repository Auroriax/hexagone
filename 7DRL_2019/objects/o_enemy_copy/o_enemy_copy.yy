{
    "id": "459a95f3-2780-4102-8d6e-847f78050c3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_copy",
    "eventList": [
        {
            "id": "f63f22df-a79e-415d-8aaa-073cc36066b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "459a95f3-2780-4102-8d6e-847f78050c3c"
        },
        {
            "id": "e590d264-0b8c-4da1-a3b5-b7aa8628d1d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "459a95f3-2780-4102-8d6e-847f78050c3c"
        },
        {
            "id": "4e47a680-588c-4485-8ae2-addd2b47dd5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "459a95f3-2780-4102-8d6e-847f78050c3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ee675dc-71f0-4d8f-989f-9c5cedfba160",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fc3543c-1a97-4acb-a02c-1985b20caec2",
    "visible": true
}