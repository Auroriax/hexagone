{
    "id": "53d5d3c0-a74d-4619-b569-17f0b16d4b54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_brokenshardr",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5869e097-189b-4761-8450-7502966deef8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc52244a-8291-483d-83a2-55e873b83028",
    "visible": true
}