{
    "id": "12c2f955-b876-4d11-b9e2-7e294a387452",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_graph_levelups",
    "eventList": [
        {
            "id": "fe5781b1-0b64-4939-aeb0-57cb1bbd1309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12c2f955-b876-4d11-b9e2-7e294a387452"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ac917a5-c103-4e1a-9946-b0cad03a53e3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
    "visible": true
}