/// @description Wiggle up and down

if irandom(2) == 1
{
	yoff = max(ymin,yoff - 1);
}
else
{
	yoff = min(ymax,yoff + 1);
}

alarm[0] = irandom_range(300,3000);