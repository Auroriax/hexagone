{
    "id": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tile_parent",
    "eventList": [
        {
            "id": "26c39d33-9c72-4d16-af36-6a00f520e17e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6"
        },
        {
            "id": "565c97b3-5e47-4bd8-84bc-66cd2ada0c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6"
        },
        {
            "id": "f9282a20-7ead-470b-9308-8eb2b6a7e7b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6"
        },
        {
            "id": "fb0892fe-2a3e-453c-891e-a49e9e823af5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "4ce14007-9af1-4627-8fa8-b7d5a6298dd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a1326931-aeac-4a3d-84f9-52c417bbd320",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff90c9b8-8a7e-4243-b840-f7e8d55e37dc",
    "visible": true
}