/// @description ACT

if !place_meeting(x,y,o_tile_parent) {instance_destroy(); exit;}

if countdown > 0
{
	countdown -= 1;
	if countdown <= 0
	{
		var l = instance_create_depth(x,y,-700,o_enemy_laser)
		l.dir = dir; l.stunned = true;
		countdown = 0;
	}
}
else
{
	var newdir = -1;
	var prec = true;
	if (collision_line(x,y,x,y-(g_height*tileheight)*2,o_player,prec,true) != noone && (dir == TOP)) 
	{newdir = TOP;}
	else if (collision_line(x,y,x-(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir == TOPLEFT) 
	{newdir = TOPLEFT;}
	else if (collision_line(x,y,x-(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir == BOTTOMLEFT) 
	{newdir = BOTTOMLEFT;}
	else if (collision_line(x,y,x,y+(g_height*tileheight)*2,o_player,prec,true) != noone && dir == BOTTOM) 
	{newdir = BOTTOM;}
	else if (collision_line(x,y,x+(g_width*tilewidth)*2,y+(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir == BOTTOMRIGHT) 
	{newdir = BOTTOMRIGHT;}
	else if (collision_line(x,y,x+(g_width*tilewidth)*2,y-(g_height*tileheight/2)*2,o_player,prec,true) != noone && dir == TOPRIGHT) 
	{newdir = TOPRIGHT;}
	else {exit}

	if newdir == dir
	{
		countdown = 3;
	}
}