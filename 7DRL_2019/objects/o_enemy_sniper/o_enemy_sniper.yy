{
    "id": "58d2070c-be4e-421d-9122-19185c6e6c68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_sniper",
    "eventList": [
        {
            "id": "0348c42d-f782-4a57-902d-69586c7f842f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "58d2070c-be4e-421d-9122-19185c6e6c68"
        },
        {
            "id": "31651dc9-0f13-400f-972b-18d12daf7765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "58d2070c-be4e-421d-9122-19185c6e6c68"
        },
        {
            "id": "53495865-13aa-4c4e-a88f-2549a3e7666f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "58d2070c-be4e-421d-9122-19185c6e6c68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1ee675dc-71f0-4d8f-989f-9c5cedfba160",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90b32b23-0985-409d-a4f1-9a6e90c8cc71",
    "visible": true
}