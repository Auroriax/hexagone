{
    "id": "f1c88553-c537-4aeb-a2dc-0934a35de594",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hexaria1",
    "eventList": [
        {
            "id": "b0c413c0-1e58-4cfa-b46b-dfdc2dadcb7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f1c88553-c537-4aeb-a2dc-0934a35de594"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5869e097-189b-4761-8450-7502966deef8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9403498c-54f4-4d2c-9369-c9b374f41ae7",
    "visible": true
}