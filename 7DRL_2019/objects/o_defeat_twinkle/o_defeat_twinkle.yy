{
    "id": "139466a6-d454-444f-ae89-efb356ff7c60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_defeat_twinkle",
    "eventList": [
        {
            "id": "19ad98e0-beb1-4eb5-b92f-e4ac01bf74d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "139466a6-d454-444f-ae89-efb356ff7c60"
        },
        {
            "id": "2f17601e-8cb4-4404-bb37-21010d0dc33b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "139466a6-d454-444f-ae89-efb356ff7c60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d2a982a-39ed-490a-8950-1483e2a8fb7c",
    "visible": true
}