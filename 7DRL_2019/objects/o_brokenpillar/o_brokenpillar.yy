{
    "id": "ed6340ed-d96a-408b-bede-7e3b2b4d4d3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_brokenpillar",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5869e097-189b-4761-8450-7502966deef8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8d2df2a-eef1-448d-988f-d1038f5b98bf",
    "visible": true
}