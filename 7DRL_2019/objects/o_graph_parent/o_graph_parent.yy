{
    "id": "1ac917a5-c103-4e1a-9946-b0cad03a53e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_graph_parent",
    "eventList": [
        {
            "id": "892be3c2-6d3b-4c22-917d-66326492bdc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ac917a5-c103-4e1a-9946-b0cad03a53e3"
        },
        {
            "id": "655a1a29-2ecc-47e0-aaf5-51daa5a30e04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1ac917a5-c103-4e1a-9946-b0cad03a53e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24eefd0e-2459-42e2-9405-b2fcf4594d19",
    "visible": true
}