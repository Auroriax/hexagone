/// @description Draw graph

var clr = c_black
var wht = c_white
mr = 10
draw_set_alpha(0.5)
draw_rectangle_color(x-mr,y-mr,x+width+mr,y+height+mr,clr,clr,clr,clr,false)
draw_set_alpha(1)

var highestdatapoint = hi;

var prevx = x; var prevy = y + height;
for(var i = 0; i != array_length_1d(data); i += 1)
{
	var xx = x+(width*(i/array_length_1d(data)));
	var yy = y+height-(height*(max(data[i],0)/highestdatapoint));
	
	if data[i] != -1
	{
		var wdt = 1; if i == array_length_1d(data)-1 {wdt = 2}
	
		if i != 0 && data[i-1] != -1
		{
			draw_line_width(prevx,prevy,xx,yy,wdt)
		}
	
		var sprt = s_hexaria1; 
		if i == array_length_1d(data)-1 {sprt = s_hexaria3}
		draw_sprite(sprt,0,xx-1,yy-3)
	}
	
	prevx = xx; prevy = yy;
}

draw_sprite(sprite_index,image_index,x,y-26)

draw_text(x+70-25,y-30,string(data[9]))

draw_sprite(s_trophy,0,x+width-50,y-26)
draw_set_halign(0); draw_set_valign(0);
draw_text(x+width-35,y-30,string(hi))